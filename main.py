"""Aplicaçao do FastApi."""
from fastapi import FastAPI
from starlette.requests import Request
from starlette.templating import Jinja2Templates
from schema_functions import init_BancoDeDados

app = FastAPI()
app.db = init_BancoDeDados()

templates = Jinja2Templates(directory="templates")


@app.get('/')
def index(request: Request):
    """Página Principal da aplicação."""
    inventario = app.db.all()
    response = {'request': request, 'inventario': inventario}

    return templates.TemplateResponse('index.html', response)
