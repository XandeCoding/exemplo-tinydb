"""Funções do Banco de dados."""
from tinydb import TinyDB
from schema import Item


def init_BancoDeDados():
    """Inicializa Banco de dados."""
    db = TinyDB('tinydb/db.json')
    # popula_BancoDeDados(db)

    return db


def popula_BancoDeDados(database):
    """Adiciona itens ao banco de dados."""
    item_1 = Item(nome='Mouse', categoria='Periféricos', quantidade=1)
    item_2 = Item(nome='Processador', categoria='Peças', quantidade=5)
    item_3 = Item(nome='Pasta Térmica', categoria='Manutenção', quantidade=2)

    database.insert(dict(item_1))
    database.insert(dict(item_2))
    database.insert(dict(item_3))
