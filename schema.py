"""Esquema dos itens do inventário, persistidos no Banco de dados."""

from pydantic import BaseModel


class Item(BaseModel):
    """
    Classe Base de item.

    atributos: nome, categoria, quantidade.
    """

    nome: str
    categoria: str
    quantidade: int
